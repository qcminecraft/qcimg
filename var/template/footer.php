<?php
if(!defined("__ROOT_DIR__")){
    http_response_code(500);
    exit("error: 500");
}
?>
<footer>
    <div class="container">
            <p class="text-muted middle" style="position:absolute;bottom:5px;">Powered by Famio & <a href="https://www.qcminecraft.com" target="_blank" >QCMinecraft</a></p>
    </div>
</footer>
    <script type="text/javascript" src="static/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="static/js/zui.min.js"></script>
    <script type="text/javascript" src="static/js/loading.js"></script>
    <script type="text/javascript" src="static/js/main.js"></script>
</body>
</html>