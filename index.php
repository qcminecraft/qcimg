<?php
/**
 *   ___   ____ ___
 * / _ \ / ___|_ _|_ __ ___   __ _  __ _  ___
 *| | | | |    | || '_ ` _ \ / _` |/ _` |/ _ \
 *| |_| | |___ | || | | | | | (_| | (_| |  __/
 * \__\_\\____|___|_| |_| |_|\__,_|\__, |\___|
 *                                  |___/
 */
if (!defined('__ROOT_DIR__') && !@include_once 'config.php') {
    if(file_exists('./install.php')){
        header('Location: install.php');
    }else{
        echo('error: file incomplete');
    }
    exit();
}
?>