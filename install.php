<?php
/**
 *   ___   ____ ___
 * / _ \ / ___|_ _|_ __ ___   __ _  __ _  ___
 *| | | | |    | || '_ ` _ \ / _` |/ _` |/ _ \
 *| |_| | |___ | || | | | | | (_| | (_| |  __/
 * \__\_\\____|___|_| |_| |_|\__,_|\__, |\___|
 *                                  |___/
 * qcimg - install.php
 * Copyright (c) 2015 - 2018.,QCTech ,All rights reserved.
 * Created by: QCTech
 * Created Time: 2018-09-02 - 17:52
 */
if (file_exists(dirname(__FILE__) . '/config.php')){
    exit("Error: config already exists");
}