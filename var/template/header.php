<?php
if(!defined("__ROOT_DIR__")){
    http_response_code(500);
    exit("error: 500");
}
?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="static/css/zui.min.css" />
    <link rel="stylesheet" href="static/css/loading.css" />
    <meta name="description" content="<?php echo $CONFIG['site']['description']; ?>" />
    <meta name="keywords" content="<?php echo $CONFIG['site']['keywords']; ?>" />
    <title><?php echo $CONFIG['site']['name']." - ".$CONFIG['site']['description']; ?></title>
    <?php echo $D['user_head_content']; ?>
</head>
